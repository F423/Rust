# ForLackOfABetterRust

## The Rust Programming Language book (by Steve Klabnik and Carol Nichols, with contributions from the Rust community)

put simply in a repository for educational use.

#### OS Specifications: Used Vim in a Debian Stretch 9.5 (stable) desktop install  running i3wm/xfce4.

### Install
	- run: `curl https://sh.rustup.rs -sSf | sh`
	- Add to PATH:
		- current session: run `source $HOME/.cargo/env`
		- future user sessions: add `export PATH="$HOME/.cargo/bin:$PATH"` to .bash_profile
	- Make sure you have some form of a linker. You most likely will have one pre-installed. 
		Yet, seems like Debian doesn't come with one:
			install the GNU C Compiler (GCC) using:
			`sudo apt install gcc`


---

### Documentation
Offline documentation ca
n be accessed by running `rustup doc`

### Projects

Here we go through every project. Make sure to change your working directory to each of the following projects' directory before compiling.
#### 1. hello_world:
	As the tradition goes, outputs:
		`Hello, world`
	compile with:
		`rustc main.rs`
	run with:
		`./main`

	
	
